
/* Recuperaación de elementos del DOM */
let $cta = document.getElementById("cta");
const $numberCards = document.getElementById("number-cards");
let $cardsContainer = document.getElementById("cards__container");
let counter = 1;
let cardCreated = "";

$cta.addEventListener("click", () => {

  $cardsContainer.innerHTML = "";
  let valueSelected = $numberCards.value;
  for(let i = 0; i < valueSelected; i++){
    cardCreated += `
    <div id="card" class="card">${counter} </div>
    `;
    counter++;
  }
  $cardsContainer.innerHTML = cardCreated;
});


